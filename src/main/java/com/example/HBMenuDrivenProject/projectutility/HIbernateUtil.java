package com.example.HBMenuDrivenProject.projectutility;

import com.example.HBMenuDrivenProject.entities.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;


import java.util.Properties;

public class HIbernateUtil {
public static SessionFactory sessionFactory;
 static {
     System.out.println("---------------------------------------------");
     Configuration configuration=new Configuration();
     // Hibernate settings equivalent to hibernate.cfg.xml's properties
     Properties settings = new Properties();
     settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
     settings.put(Environment.URL, "jdbc:mysql://localhost:3306/amar");
     settings.put(Environment.USER, "root");
     settings.put(Environment.PASS, "mysql");
     settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
     settings.put(Environment.SHOW_SQL, "true");
     settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
     settings.put(Environment.HBM2DDL_AUTO, "update");

     configuration.setProperties(settings);
     configuration.addAnnotatedClass(Employee.class);//we have to add all class that is table in db
     //create serviceRegistryBulder
     StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder();
    //create Service Registry
     StandardServiceRegistry serviceRegistry=builder.applySettings(configuration.getProperties()).build();

     sessionFactory = configuration.buildSessionFactory(serviceRegistry);
 }
 public static SessionFactory getSessionFactory(){
     return sessionFactory;
 }
 public static Session getSession(){
     Session session=null;
     if(sessionFactory != null){
         session=sessionFactory.openSession();
     }
     return session;
 }
}
