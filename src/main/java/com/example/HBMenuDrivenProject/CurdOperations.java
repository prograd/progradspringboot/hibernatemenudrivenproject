package com.example.HBMenuDrivenProject;

import com.example.HBMenuDrivenProject.entities.Employee;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Scanner;

public class CurdOperations {
    public static void displayAllData(Session session){
        try{
            System.out.println("All Employee Details:");
             Query query=session.createQuery("FROM Employee");
            List<Employee> list=query.list();
            for (Employee obj:list) {
                System.out.println(obj.getId()+" "+obj.getName()+" "+obj.getSalary()+" "+obj.getDepartment());
            }
        }catch (HibernateException e){
            e.printStackTrace();
        }
    }
    public static void saveData(Session session){
        Transaction tx=null;
        try {
            tx=session.beginTransaction();
            Employee employee = new Employee();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter employee name: ");
            employee.setName(scanner.nextLine());
            System.out.println("Enter Department:");
            employee.setDepartment(scanner.nextLine());
            System.out.println("Enter Salary:");
            employee.setSalary(scanner.nextFloat());

            session.save(employee);

            session.getTransaction().commit();
            System.out.println("Record Saved");
        }catch (HibernateException e){
            tx.rollback();
            e.printStackTrace();
            System.out.println("Record not Inserted/saved");
        }
    }
    public static void delete(Session session){
        Transaction tx=null;
        try {
            tx=session.beginTransaction();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter Id of employee to delete");
            Employee employee = (Employee) session.get(Employee.class,scanner.nextInt());
            session.delete(employee);

            session.getTransaction().commit();
            System.out.println("record deleted");
        }catch (HibernateException e){
            tx.rollback();
            e.printStackTrace();
            System.out.println("Record not deleted");
        }
    }
    public static void update(Session session){
        Transaction tx=null;
        try {
            tx = session.beginTransaction();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter employee Id to update");
            Employee employee = (Employee) session.get(Employee.class, scanner.nextInt());
            scanner.nextLine();
            System.out.println("Enter name:");
            employee.setName(scanner.nextLine());

            System.out.println("Enter Department:");
            employee.setDepartment(scanner.nextLine());
            System.out.println("Enter salary");
            employee.setSalary(scanner.nextFloat());

            session.saveOrUpdate(employee);

            session.getTransaction().commit();
            System.out.println("record updated");

        }catch (HibernateException e){
            tx.rollback();
            e.printStackTrace();
            System.out.println("Record not updated");
        }

    }
}
