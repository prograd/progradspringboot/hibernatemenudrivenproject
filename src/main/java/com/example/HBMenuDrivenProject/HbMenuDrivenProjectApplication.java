package com.example.HBMenuDrivenProject;

import com.example.HBMenuDrivenProject.projectutility.HIbernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

import static java.lang.System.exit;

@SpringBootApplication
public class HbMenuDrivenProjectApplication {

	public static void main(String[] args) {
		//SpringApplication.run(HbMenuDrivenProjectApplication.class, args);
		SessionFactory sessionFactory= HIbernateUtil.getSessionFactory();
		Session session=HIbernateUtil.getSession();
		Scanner scanner=new Scanner(System.in);
		int choice=0;
           try {
			   do {
				   System.out.println("***********MENU************");
				   System.out.println("1.Insert new employee\n2.Display all employee\n3.Update Employee Details\n4.Delete employee\n5.EXIT");
				   System.out.println("***********Enter your choice************");
				   choice = scanner.nextByte();
				   if(choice==1){
					   CurdOperations.saveData(session);
				   } else if (choice==2) {
					   CurdOperations.displayAllData(session);
				   }else if (choice==3) {
                       CurdOperations.update(session);
				   }else if (choice==4) {
					   CurdOperations.delete(session);
				   }else if (choice==5) {
					   exit(0);
				   }else
					   System.out.println("Invalid option");

			   }while(true);

		   }catch (HibernateException e){
			   e.printStackTrace();
		   }
		session.close();
		sessionFactory.close();
	}

}
